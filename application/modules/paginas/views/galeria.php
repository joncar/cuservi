<?php

$page = $this->load->view($this->theme.'galeria',array(),TRUE,'paginas');

$galeria = $this->db->get('galeria');
$this->db->order_by('orden','ASC');
$this->db->select('galeria_fotos.*, galeria.nombre, galeria.id as cat');
$this->db->join('galeria','galeria.id = galeria_fotos.galeria_id');
$fotos = $this->db->get_where('galeria_fotos');
foreach($fotos->result() as $n=>$f){
	$fotos->row($n)->foto = base_url('img/galeria/'.$f->foto);
}
$page = $this->querys->fillFields($page,array('galeria'=>$galeria,'fotos'=>$fotos));
$page = $this->load->view('read',array('page'=>$page),TRUE,'paginas');
echo $page;
?>
