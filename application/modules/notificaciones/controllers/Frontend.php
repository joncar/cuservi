<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function ganadores(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('ganadores')
                 ->set_subject('Subscritos')
                 ->set_theme('bootstrap2')
                 ->unset_add()               
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->order_by('id','DESC')
                 ->edit_fields('intentos','premio','visto')
                 ->columns('id','nombre','email','telefono','intentos','premio','codigo')
                 ->callback_after_update(function($post,$primary){                    
                 if(!empty($post['visto'])){
                    $envio = $this->db->get_where('ganadores',array('id'=>$primary))->row();                                        
                    correo('sonia@cuservi.com',$envio->email.' ha clickado el boletin',$envio->email.' Ha visto el boletin');                                                        
                 }
                 elseif(!empty($post['premio'])){
                    $email = get_instance()->db->get_where('ganadores',array('id'=>$primary))->row()->email;
                    correo($email,'Premio recibido','Hola el jugador '.$email.' Ha recibido el premio '.$post['premio']);
                    correo('info@curservi.com','Premio recibido','Hola el jugador '.$email.' Ha recibido el premio '.$post['premio']);                    
                 }
                 elseif(isset($post['intentos']) && $post['intentos']==0){
                    $email = get_instance()->db->get_where('ganadores',array('id'=>$primary))->row()->email;
                    correo('sonia@cuservi.com',$email.' Ha perdido',$email.' Ha jugado pero no ha obtenido ningún premio');                                                          
                 }
            });
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function subscritos(){
            $this->load->library('grocery_crud');
           	$this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('emails')
            	 ->set_subject('Subscritos')
            	 ->set_theme('bootstrap2')
            	 ->unset_add()            	 
            	 ->unset_delete()
            	 ->unset_read()
            	 ->unset_print()
            	 ->unset_export()
            	 ->edit_fields('intentos','premio','visto')
                 ->columns('id','nombre','email','telefono','intentos','premio','codigo')
            	 ->callback_after_update(function($post,$primary){
                 if(!empty($post['visto'])){
                    $envio = $this->db->get_where('emails',array('id'=>$primary))->row();                    
                    correo('sonia@cuservi.com',$envio->email.' ha clickado el boletin',$envio->email.' Ha visto el boletin');                                                        
                 }
            	 if(!empty($post['premio'])){
            	 	$email = get_instance()->db->get_where('emails',array('id'=>$primary))->row();
            	 	correo($email,5);
            	 	correo($email,5,'info@curservi.com');                    
            	 }
                 if(isset($post['intentos']) && $post['intentos']==0){
                    $email = get_instance()->db->get_where('emails',array('id'=>$primary))->row();
                    correo('sonia@cuservi.com',$email.' Ha perdido',$email.' Ha jugado pero no ha obtenido ningún premio');                                                          
                 }
        	});
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function getImage($tipo,$cod,$idioma = 'es'){
            ob_end_clean();
            switch($tipo){
                case 1:
                    $img = imagecreatefrompng('boletin/'.$idioma.'/image-1.png');                                        
                    Imagettftext($img, 40, 0, 950,1780,ImageColorAllocate($img,0,0,0), './boletin/ptsans.ttf',$cod);
                break;
                case 2:
                    $img = imagecreatefrompng('boletin/'.$idioma.'/image-2.png');            
                    Imagettftext($img, 40, 0, 280,1490,ImageColorAllocate($img,0,0,0), './boletin/ptsans.ttf',$cod);
                break;
            }                     
            header("Content-Type: image/png");
            imagepng($img);
        }

        function verBoletin($id,$cod = ''){
            $boletin = $this->db->get_where('notificaciones',array('id'=>$id));
            if($boletin->num_rows()>0){
                $boletin = $boletin->row();
                $text = file_get_contents('boletin/content.html');
                $text = str_replace('image-1.png',base_url('notificaciones/frontend/getImage/1/CUS-1209211'),$text);
                $text = str_replace('image-2.png',base_url('notificaciones/frontend/getImage/2/CUS-1209211'),$text);
                $text = str_replace('box-bg-5.jpg',base_url('boletin/box-bg-5.jpg'),$text);
                $text = str_replace('[link]',base_url().'?cod=CUS-1209211',$text);
                $text = str_replace('content.html',base_url('notificaciones/frontend/verBoletin/'.$id),$text);                
                $text = str_replace('{email}',base_url('paginas/frontend/unsubscribe/'.$cod),$text);
                echo $text;
            }
        }
    }
?>
