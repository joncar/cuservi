<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function boletin(){
            $this->as['boletin'] = 'notificaciones';
            $crud = $this->crud_function('',''); 
            $crud->add_action('Enviar como boletin','',base_url('notificaciones/admin/sendMail').'/');
            $crud->unset_add()
                 ->unset_edit()
                ->unset_delete()
                ->unset_print()
                ->unset_export()
                ->unset_read();            
            $this->loadView($crud->render());
        }

        function subscritos(){
            $this->as['subscritos'] = 'emails';
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='add'){
                $crud->field_type('codigo','hidden','')
                     ->field_type('intentos','hidden',2)
                     ->field_type('premio','hidden','')
                     ->field_type('visto','hidden',0);
                $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            }
            $crud = $crud->render();
            $crud->output = '<a href="'.base_url('notificaciones/admin/importar').'" class="btn btn-info">Importar</a>'.$crud->output;
            $this->loadView($crud);
        }    

        function ganadores(){            
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='add'){
                $crud->field_type('codigo','hidden','')
                     ->field_type('intentos','hidden',2)
                     ->field_type('premio','hidden','')
                     ->field_type('visto','hidden',0);
                $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));                
            }
            $crud->set_subject('Jugadores');
            $crud = $crud->render();            
            $crud->title = 'Jugadores';
            $this->loadView($crud);
        }     

        function importar($x = '',$y = ''){
            if($x == 'procesar' && is_numeric($y)){
                $this->procesarFichero($y);
                die();
            }
            $crud = $this->crud_function('','');
            $crud->set_field_upload('fichero','files');
            if($crud->getParameters()!='list'){
                $crud->display_as('fichero','Fichero en XLSX')
                     ->display_as('sustituir','Limpiar lista y reemplazar por este fichero');
            }
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            $crud->add_action('<i class="fa fa-refresh"></i> Procesar','',base_url('notificaciones/admin/importar/procesar/').'/');
            $crud = $crud->render();            
            $crud->output = '<div class="alert alert-info"><i class="fa fa-info-circle"></i> Se debe subir el fichero y luego en acciones indicar su procesamiento</div>'.$crud->output;
            $this->loadView($crud);
        }  

        function procesarFichero($id){
            $fichero = $this->db->get_where('importar',array('id'=>$id));
            if($fichero->num_rows()>0){
                $fichero = $fichero->row();
                $file = 'files/'.$fichero->fichero;
                require_once APPPATH.'libraries/Excel/SpreadsheetReader.php';
                $reader = new SpreadsheetReader($file);
                //Se dbe limpiar los datos?
                if($fichero->sustituir==1){
                    $this->db->query('truncate emails');
                }
                foreach($reader as $data){
                    if(!empty(trim($data[0])) && $this->db->get_where('emails',array('email'=>trim($data[0])))->num_rows()==0){
                        $this->db->insert('emails',array(
                            'email'=>$data[0],
                            'nombre'=>$data[1],
                            'telefono'=>$data[2],
                            'intentos'=>2,
                            'idioma'=>$fichero->idioma
                        ));
                        $in = $this->db->insert_id();

                        do{$codigo = 'CUS-'.rand(10,99).date("hsi").$in.$this->db->get_where('ganadores')->num_rows();}
                        while($this->db->get_where('ganadores',array('codigo'=>$codigo))->num_rows()>0);

                        $this->db->update('emails',array('codigo'=>$codigo),array('id'=>$in));

                        $this->db->insert('ganadores',array(
                            'email'=>$data[0],
                            'nombre'=>$data[1],
                            'telefono'=>$data[2],
                            'intentos'=>2,
                            'idioma'=>$fichero->idioma,
                            'codigo'=>$codigo,
                            'fecha_envio'=>date("Y-m-d")
                        ));
                        $in2 = $in;
                        $in = $this->db->insert_id();

                        $this->db->update('emails',array('codigo'=>$codigo),array('id'=>$in2));
                        $this->db->update('ganadores',array('codigo'=>$codigo),array('id'=>$in));
                    }
                }
                redirect('notificaciones/admin/subscritos/success');
            }
        }

        function sendMail($id){
            $boletin = $this->db->get_where('notificaciones',array('id'=>$id));
            if($boletin->num_rows()>0){
                $boletin = $boletin->row();
                
                $text = file_get_contents('boletin/es/content.html');
                $text = str_replace('box-bg-5.jpg',base_url('boletin/box-bg-5.jpg'),$text);
                $correo = '';                
                foreach($this->db->get_where('emails',array('idioma'=>'es'))->result() as $e){
                    $texto = str_replace('image-1.png',base_url('notificaciones/frontend/getImage/1/'.$e->codigo.'/es'),$text);
                    $texto = str_replace('image-2.png',base_url('notificaciones/frontend/getImage/2/'.$e->codigo.'/es'),$texto);            
                    $texto = str_replace('[link]',base_url('game/index.php').'?cod='.$e->codigo.'&lang=es',$texto);
                    $texto = str_replace('{link}',base_url('notificaciones/frontend/verBoletin/'.$id.'/'.$e->codigo),$texto);
                    $texto = str_replace('{email}',base_url('paginas/frontend/unsubscribe/'.$e->codigo),$texto);                    
                    correo($e->email,'Cuservi presenta',$texto);
                    echo $texto;
                }
                

                $text = file_get_contents('boletin/en/content.html');
                $text = str_replace('box-bg-5.jpg',base_url('boletin/box-bg-5.jpg'),$text);
                $correo = '';
                foreach($this->db->get_where('emails',array('idioma'=>'en'))->result() as $e){
                    $texto = str_replace('image-1.png',base_url('notificaciones/frontend/getImage/1/'.$e->codigo.'/en'),$text);
                    $texto = str_replace('image-2.png',base_url('notificaciones/frontend/getImage/2/'.$e->codigo.'/en'),$texto);            
                    $texto = str_replace('[link]',base_url('game/index.php').'?cod='.$e->codigo.'&lang=en',$texto);
                    $texto = str_replace('{link}',base_url('notificaciones/frontend/verBoletin/'.$id.'/'.$e->codigo),$texto);
                    $texto = str_replace('{email}',base_url('paginas/frontend/unsubscribe/'.$e->codigo),$texto);                    
                    correo($e->email,'Cuservi presents',$texto);
                    echo $texto;
                }                
                echo "Boletin enviado <a href='".base_url('notificaciones/admin/notificaciones')."'>Volver</a>";          
            }
        }
    }
?>
