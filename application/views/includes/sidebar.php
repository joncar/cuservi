<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'b'=>array('blog_categorias','blog'),    
                        'entradas'=>array('admin/categorias_servicios','admin/servicios','admin/talleres','admin/categorias_libros','admin/libros','admin/categorias_productos','admin/productos','admin/formacion'),
                        'notificaciones'=>array('admin/boletin','admin/subscritos','admin/ganadores'),                        
                        'paginas'=>array('admin/paginas','admin/subscriptores','admin/galeria','admin/galeria_equipo','admin/galeria_dj','admin/galeria_influencers','admin/galeria_fiestas','admin/galeria_que_es_mif','admin/galeria_que_incluye','admin/galeria_actividades','admin/galeria_resort','admin/galeria_excursiones','admin/trabaja_con_nosotros','admin/concurso','admin/catalogo'),
                        'seguridad'=>array('acciones','ajustes','cookies','grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'b'=>array('Blog','fa fa-book'),                         
                        'paginas'=>array('Paginas','fa fa-file-powerpoint-o'), 
                        'notificaciones'=>array('Juego','fa fa-file'),  
                        'servicios'=>array('Terapias',''),
                        'categorias_servicios'=>array('Categoria de terapias'),  
                        'ganadores'=>array('Jugadores',''),                   
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">EVA software</a>
        </div>
        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
