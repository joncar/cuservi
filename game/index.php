<?php if(!empty($_GET['cod'])): ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>Cuservi Roulette</title>        
        <link rel="shortcut icon" href="icon.ico" type="image/x-icon">        
        <link rel="stylesheet" href="css/main.css">        
        <style>
            html,body,p,h1{
                font-family:'libel_suitregular';
            }
        </style>
    </head>
    <body>
        <?php             
            if(empty($_GET['lang']) || $_GET['lang']=='es'): 
        ?>
        <div class="texto">
            <h1>Aviso Legal</h1>
            <div class="cuadro">                
                <p style="text-align: justify;">
                    El nuevo Reglamento europeo de protección de datos personales. En consecuencia, hemos actualizado nuestra política de privacidad para adecuarla a la nueva normativa y garantizar a todos nuestros clientes un uso adecuado y transparente de sus datos. Por eso te pedimos que leas detenidamente la información que hemos incluido en este correo. Si quieres ampliarla, puedes dirigirte a la web:                    
                    <a href="http://www.cuservi.com/mor20.html" target="_new">Más información</a>
                </p>
            </div>
            <div style="margin-top:40px">
                <div style="display: none; color:red;" id="msj">Por favor acepta nuestras políticas de privacidad</div>
                <label><input type="checkbox" value="1" id="acepto"> Acepto las condiciones y politicas de privacidad</label>
            </div>
            
            <div style="text-align: center">
                <img src="assets/button_start.png" alt="" id="jugar">
            </div>
        </div>
        <?php else: ?>
        <div class="texto">
            <h1>LEGAL ADVICE</h1>
            <div class="cuadro">                
                <p style="text-align: justify;">
                    Based on the new European Regulation of Personal Data Protection, we have updated our privacy policy to adapt it to the new rule and to guarantee to all our clients the adequate and transparent use of their data. Therefore, we ask you to read carefully the information that we have included in this email. If you wish to see more extended details, you can go to our webpage:
                    <a href="http://www.cuservi.com/mor20.html" target="_new">More information</a>
                </p>
            </div>
            <div style="margin-top:40px">
                <div style="display: none; color:red;" id="msj">Please accept our privacy policies</div>
                <label><input type="checkbox" value="1" id="acepto"> I accept the conditions and privacy policies</label>
            </div>
            
            <div style="text-align: center">
                <img src="assets/button_start.png" alt="" id="jugar">
            </div>
        </div>
        <?php endif ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.4.min.js"><\/script>')</script>
        <script>
            var URL = 'http://<?= $_SERVER['HTTP_HOST'] ?>/ruleta/notificaciones/frontend/';
            $(document).on('ready',function(){

                $.post(URL+'ganadores/json_list',{
                    'search_text[]':'<?= $_GET['cod'] ?>',
                    'search_field[]':'codigo',                    
                },function(data){
                    
                    data = JSON.parse(data);
                    data = data[0];
                    player = data;      
                    console.log(player);            
                    gameChance = parseInt(player.Intentos);
                    var codigo_juego = data.Id;
                    var datos = new FormData();
                    datos.append('visto',1);                    
                    remoteConnection('ganadores/update/'+player.Id,datos,function(){});
                });
            });


            $("#jugar").on('click',function(){
                if($("#acepto").prop('checked')){
                    document.location.href="game.php?cod=<?= $_GET['cod'] ?><?= @'&lang='.$_GET['lang'] ?>";
                }else{
                    $("#msj").show();
                }
            });

            function remoteConnection(url,data,callback){

                $.ajax({
                    url: URL+url,
                    data: data,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:callback
                });
            };
        </script>
    </body>
</html>
<?php endif ?>