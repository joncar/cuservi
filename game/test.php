<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cuservi Roulette</title>
        
        <meta name="Title" content="Lucky WHeels" />
        <meta name="description" content="Lucky Wheels is a HTML5 game where you spin the wheel to win the points, it come with 2 wheels where it give a second chance to bonus up the score or loss it all.">
		<meta name="keywords" content="fortune, wheel, segment, point, gamble, jackpot, lose, lucky, bingo, spin, slots, wins">
        
        <!-- for Facebook -->
        <meta property="og:title" content="CUSERVI ROULETTE"/>
        <meta property="og:site_name" content="CUSERVI ROULETTE"/>
        <meta property="og:image" content="http://www.cuservi.com/ruleta/game/share.jpg" />
        <meta property="og:url" content="http://www.cuservi.com/ruleta/game/index.php" />
        <meta property="og:description" content="Lucky Wheels is a HTML5 game where you spin the wheel to win the points, it come with 2 wheels where it give a second chance to bonus up the score or loss it all.">
        
        <!-- for Twitter -->
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Lucky WHeels" />
        <meta name="twitter:description" content="Lucky Wheels is a HTML5 game where you spin the wheel to win the points, it come with 2 wheels where it give a second chance to bonus up the score or loss it all." />
        <meta name="twitter:image" content="http://demonisblack.com/code/luckywheels/game/share.jpg" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement("style");
            msViewportStyle.appendChild(
                document.createTextNode(
                    "@-ms-viewport{width:device-width}"
                )
            );
            document.getElementsByTagName("head")[0].
                appendChild(msViewportStyle);
        }

        </script>

        <link rel="shortcut icon" href="icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/normalize.css">        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!-- CANVAS START-->
        <div id="canvasHolder">
            <canvas id="gameCanvas" width="1024" height="600"></canvas>
        </div>
        <!-- CANVAS END-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.4.min.js"><\/script>')</script>
        
        <script src="js/vendor/detectmobilebrowser.js"></script>
        <script src="js/vendor/createjs-2015.11.26.min.js"></script>
        <script src="js/vendor/p2.min.js"></script>
		<script src="js/vendor/TweenMax.min.js"></script>
        <script>

        	var confetti = function(){
        		var items = [];
				var colors = ['#3788c7', '#f298be', '#f6e25b', '#8a5bac', '#00a944', '#ec2a28', '#43c4c0', '#f4b92f'];
				var index = 300;
				this.triggered = false;
				var width, height;
				var first = 0;
				var content = undefined;

				width = canvasW;
	  			height = canvasH;

				var startTime = new Date().getTime();
				var currentTime = startTime;	

				this.render = function(){
				  var time = new Date().getTime();
				  var delta = (time - currentTime) / (80000/60);

				  
				  /*if(time - startTime > 1500){
				    items[++index % 600] = particle(canvasW/100 * 33, canvasH/100 * 12, index, 20);
				  }*/

				  items.forEach(function(item){
				    item.vx *= (1.0 - 0.05*delta);
				    item.vy += (delta * item.fallSpeed);
				    item.vy /= 1.0 + 0.05*delta;
				    var x = item.x+(delta * item.vx);
				    var y = item.y+(delta * item.vy);
				    var rotate = item.rotate+(delta * item.direction);

				    TweenMax.to(item,item.fallSpeed,{x:x,y:y,rotate:rotate});
				  });				 
			  	  

				}

				function particle(x, y, i, minv){
				  var angle = Math.random()*(Math.PI*2);
				  var amount = Math.random()*10 + minv;	  
				  var vx = Math.sin(angle) * amount;
				  var vy = Math.cos(angle) * amount;
				  
				  var prop = {
				    x: x,
				    y: y,
				    vx: vx,
				    vy: vy,
				    width: (Math.random() * 10) + 4,
				    height: (Math.random() * 10) + 4,
				    color: colors[i % colors.length],
				    circle: (Math.random() > 0.8),
				    rotate: Math.random() * 180,
				    direction: (Math.random() * 5) - 2.5,
				    fallSpeed: (Math.random() / 10) + 0.1
				  }

				  var element = new createjs.Shape().set(prop);	
				  element.graphics.f(colors[i % colors.length]).rr(0,0,(Math.random() * 10) + 4,(Math.random() * 10) + 4,0);				       			      			      
			      return element;
				}

				this.triggerConfetti = function(stage){
					content = stage;
					this.triggered = true;
					for(var i = 0; i < index; i++){
						var element = particle(canvasW/100 * 33, canvasH/100 * 12, i, 5);
						content.addChild(element);
					    items[(index + 150 + i) % 600] = element;					    
					}
					index = (index + 150) % 600;
				}

				
        	};


    		function tick(event) {
    			conf.render();
			    stage.update(event);

			}


			var stageW=1024;
			var stageH=600;
			var contentW = 1024;
			var contentH = 600;
			var canvasW = 1024;
			var canvasH = 600;

			var stage = new createjs.Stage("gameCanvas");
			
			var conf = new confetti();
			conf.triggerConfetti(stage);
			
			createjs.Ticker.on("tick", tick);
			
			/*var s = new createjs.Shape().set({x:0, y:0});
			s.graphics.f("red").rr(0,0,100,100,0);
			stage.addChild(s);*/



        </script>
    </body>
</html>