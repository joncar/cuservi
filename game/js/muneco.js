(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.Interpolación3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EBE7E9").s().p("AGVI+IqChBQgzgFgsgcQgsgcgbgsQgng/AHhMQADgdAKgbIERrOQAPgmAlgRQAmgRAmAPQAeALASAdQASAdgEAhQAAANgGAMIkQLOIgBAGQgBAJAEAHQAHAKALABIKCBBQAFABAEAFQAEAEgBAGIgRCoQAAAGgFAEQgEADgFAAIgBAAg");
	this.shape.setTransform(-88.7,-112.5,1.23,1.23);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F69789").s().p("AhwDXQhHAAgxgmQgxgnABg2QABgxAqgjIA1gyIA1gzQAigkAZg6QAGgMAMgFQAMgFANAFQAMAGAFAMQAEAMgFAMQgTAsgaAkQgKANAHAOQAIAPAPAAIAZACQBHALBGgFIB6gJQAMgBAJAIQAJAIABAMQABAMgIAJQgIAIgMABIinAMQgMABgHAJQgHAJACALQASB7hLAfQgYALgnABIg3gBg");
	this.shape_1.setTransform(-3.2,-62.6,1.23,1.23);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F69789").s().p("AALBTIhsgLIAQibIBsAMQAgADAVAZQAUAZgDAfQgEAggZAVQgVASgaAAIgKgBg");
	this.shape_2.setTransform(-24.9,-53.2,1.23,1.23);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F37D7D").s().p("AgUBgQgfgegOgxQgPgwAJgnQAJgoAbgHQAcgHAcAdQAeAeAPAxQAPAwgKAoQgJAngbAHIgKACQgXAAgWgYg");
	this.shape_3.setTransform(-70.8,-255.2,1.23,1.23);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#441A38").s().p("AhtA7QgHgDABgMIABgBIAFguIADgSIACgIIADgHIAEgFIAEgCIABAAIAAgBIDMgOIAEAwIgLAAQgHABgOgQQgPgQgKAAIiUALIgCABIgCABIgBADQgDAIgBASIgCAxQgBAFgCADQgBAAAAAAQAAABgBAAQAAAAgBAAQAAAAgBAAIgCAAg");
	this.shape_4.setTransform(-67.5,-261.4,1.23,1.23);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#441A38").s().p("AlOB8QgVgSgJgfIgIgkQgJgMgLgJIgIgHIgEg6IAtgeQA+gdBWABQBRABBBAUQA8ASAFAAQACAAA6gbQA+gdBQgNQBUgOBCAUQAhAKAQANIAEA8QgMALgLAUQgBAYgCAMQgFAggRAVQgTAWgYACIiGAPIgDAAQghACgdgWQgcgWgPgmIgPgeQgEgJgOgGQglgPgjAVQgMAHgDAKQgEAOgHARQgJAogZAaQgZAaghADIiJAFIgDAAQgXAAgUgSgAjohWIgNAAQg+AFgfAXQgYASAAAVQAAAmAPAzQAHAaAQAOQAQAPATgBICGgEIADgBQAigDAWggQAYggAAguQAAgsgUgUQgcgdhZAAIgXABgADth5IgNACQhrALgbAlQgSAWAIAtQAHAsAcAdQAbAdAigCICIgQQATgCANgRQAOgRAEgZQAHgzgGgnQgDgVgagOQgdgQgvAAIgVABg");
	this.shape_5.setTransform(-13.1,-263.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B66149").s().p("AnUDqQgyhtABhSQABgnANgiQAOgmAagaQAfgdApgIQAIgCAGgGQAFgGAAgJQgCgrAlgwQAfgoA/gtQArgfBHgSQAtgLBPgKQBogMBPANQBHANA4AWQBxAuA1BCQAhAqAKAyQAKA2gUAsQgLgsgOgdQgQghgYgYQAWAxgCA2QgDBDgpA3QgKgvgbgqQgcgqgngdIgGAAQgXACgfAdQgjAggRAGQgoAQhCgkQgkgUgOgGQgegLgXADQgeADgpAfIgfAZQgTAMgQAGIgGACQAAAAAAAAQAAAAgBABQAAAAAAAAQAAAAAAABIgBACIACAFQAJApgoAzQgpA1ANAqQABAEAPAfQAOAfgJAfQgFARgXAoQgIgfgHgQQgKgagPgQQgSgUgagHQgcgGgVAOQghAXADBDQACAhAEAWQADARALAkQg/hOgrhfg");
	this.shape_6.setTransform(-34.5,-295.5,1.23,1.23);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#9C4F43").s().p("AAqAVQgQgKgfgBQgXgBgTAEQgHACgGgEQgGgEgBgHQgCgGAEgHQAEgFAHgCQAWgEAXAAQAsABAaARQAGADABAGQACAIgEAGQgFAGgJABQgFAAgFgDg");
	this.shape_7.setTransform(8.5,-278.7,1.23,1.23);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#9C4F43").s().p("AhMAeQgGgHAAgJQABgIAHgGQAcgZA1gIQAegEAZABQAJABAGAGQAGAHAAAJQgBAIgGAFQgHAGgJAAQgXgBgcAEQglAHgSAQQgGAFgHAAQgLAAgGgHg");
	this.shape_8.setTransform(-38.4,-278);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F4847F").s().p("AACAbQgCgFgJgDQgIgDgIAAQgLgBgIgIQgHgHAAgLQABgMAIgHQAIgIALAAQANAAAQAGQAdAJANAVQAGAJgCAKQgCALgKAHQgGAEgHAAIgDAAQgOAAgIgMg");
	this.shape_9.setTransform(11.9,-244.8,1.23,1.23);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F4847F").s().p("AghAoQgKgFgEgKQgEgLAEgKQALgWAbgNQAMgHAPgDQALgCAKAGQAJAHACALQACALgGAIQgHAJgLADQgHABgIAEQgJAEgCAFQgGAPgQABIgCABQgFAAgGgDg");
	this.shape_10.setTransform(-38,-244.4,1.23,1.23);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#BE423C").s().p("Ag/ArQgfgdgIgrQgBgJAFgJQAEgHAJgBICngNQAJAAAGAHQAGAHAAAKQgBAsgbAhQgbAhgnACIgCABIgHAAQgiAAgdgag");
	this.shape_11.setTransform(-12.7,-236.2,1.23,0.838);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#44203C").s().p("AgQAgQgIgIgBgNIgBgRQgBgNAHgJQAHgJALgBQAKgBAJAIQAIAIABANIABARQABANgHAJQgHAKgLAAIgDAAQgIAAgIgHg");
	this.shape_12.setTransform(-35.2,-259.2,1.23,0.467);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#44203C").s().p("AgPAgQgJgIgBgNIgBgRQgBgNAHgJQAHgJALgBQAKgBAJAIQAIAIABANIABARQABANgHAJQgHAKgLAAIgCAAQgJAAgHgHg");
	this.shape_13.setTransform(7.1,-262.2,1.23,1.23);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F69789").s().p("ACKHfQhQgBhfghQhQgchJgxQhIgwg0g8Qg4hCgihRQghhOgHhUIgBgLQgMifB3h6QB3h6C0gNQA2gEA0AIQBQAMBHAnQBHAmAxA5QAuA2AaBMQANAnAFA0IAHBcIAIBXQAFA0gBAjQgDBNglBGQglBIhAAxQhCAyhiAAIgEAAg");
	this.shape_14.setTransform(-34.2,-268.8,1.23,1.23);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#441A38").s().p("AhtA7QgHgDABgNIAIhAIAGgPIAEgFIADgCIACAAIAAgBIDMgPIAEAxIgLAAQgHABgPgQQgPgQgJAAIiVALIgCABIgCAEQgDAIgBASIgCAxQgCAKgEAAIgDgBg");
	this.shape_15.setTransform(13.1,-265,1.23,1.23);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F8964F").s().p("Ag1BHQgJgGgEgLIgDgJIgFhOQgBgFACgFQACgKAIgIQAJgHALgBIBMgGQALAAAKAGQAJAGAEAKIACAKIAGBNIgBAKQgCALgJAHQgIAIgLAAIhNAGIgDAAQgJAAgIgFgAgpgrQgIAAABAIIAFBOQABAIAHgBIBNgGQADAAACgCQACgDAAgDIgGhOQgBgHgHAAg");
	this.shape_16.setTransform(-3.6,-20,1.23,1.23);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#754648").s().p("AgeguIAogDIAIAcIAOBDIg3AEg");
	this.shape_17.setTransform(23.7,-21.9,1.23,1.23);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#754648").s().p("Ag6gsIBugIQgCAFABAFIAGBOIACAJIhuAIg");
	this.shape_18.setTransform(-19.1,-18.9,1.23,1.23);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#754648").s().p("AgaAKIAQg5IArgEIgQBjIgxAEIAGgqg");
	this.shape_19.setTransform(-64.4,-15.6,1.23,1.23);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#754648").s().p("AhdgqIDEgOIAHBhIjbAQg");
	this.shape_20.setTransform(-44.4,-17,1.23,1.23);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#754648").s().p("AgnAqIgGhNIgCgKIBYgGIAHBhIhXAGIAAgKg");
	this.shape_21.setTransform(10.5,-21,1.23,1.23);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#754648").s().p("AgqArIgGhOQgBgIAIAAIBNgGQAHAAABAHIAFBOQABADgDADQgBACgEAAIhMAGIgCAAQgGAAAAgHg");
	this.shape_22.setTransform(-3.6,-20,1.23,1.23);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#453C5A").s().p("Ak8DKQhGhSgJh/QgFhOAShOQAIgnAPgnIACgGQAEgLALgBIKsgxQAHgBAFAEQAFAEABAGQAHAfAIAbQANAuADAuIAAAGQAIBrgDAxQgFBSgfA3QgoBFhVAlQhVAmiMAKQgZACgZAAQi3AAhdhsg");
	this.shape_23.setTransform(-20.2,4.6,1.23,1.23);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D1495C").s().p("ABIJKIg9hQQgFgHAAgJQALkxgXk1QgOirgUiJQgDgMgIgGIhahJQAkhAAdgDQAigCAsAvIgCBbQAAAHABAIQAgBZAgCkQA2EdAXF/QAAAHgDAGIgvBaQgDAGgGAAIgCAAQgFAAgEgFg");
	this.shape_24.setTransform(-11,-117.1,1.23,1.23);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#C9BDC6").s().p("AgqBSQhzhGhPhgQBVBVB5A8QAGADAHgDQAHgCACgHQAQgqASgfQAhg5AagIIAOgBQAjADAvA0QAZAcAcAqIADABQABAegKABQgBAAgxhIQgyhJgeACIgMABIABACIgFgBQgPAKgUAsQgOAcgQAwQgFASgQAIQgHADgHAAIgEAAQgKAAgKgGg");
	this.shape_25.setTransform(-31.6,-178.9,1.23,1.23);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#DEDBE1").s().p("AjPL2QgFgGACgFIAGgfQAHgkAQgqQAth4gJiAQgJiBg+hxQgbgygjhJIgthYQhBh5gdhrQgchshNhVIgLgOQgWgkAKgoQAKgoAjgVQBshCDCgjIABAAQAzBABSA4QA0AkA1AbQAGACAHgCQAHgDACgGQARgqASghQAkg/AdgDQAlgDA1A6QAZAcAcArQAAABABAAQAAABAAAAQABAAAAAAQABAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQgHglgTgnQgcg7gxgvQCyAKBnAsQAlARAQAlQAPAmgQAmQguBrALB1QALBzgDB0QgDB0gPBeQgXCLAICMQAICOAlCFQAIAeADAXQACAGgEAEQgDAEgGAAIrTA1QgGAAgEgEg");
	this.shape_26.setTransform(-40.5,-103.2,1.23,1.23);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#EBE7E9").s().p("ABKBdQgdgrgZgcQgzg5glACIACgOQgBgTgLgbIA2ACQAwAvAdA6QATAnAHAlQAAABAAAAQAAABAAAAQAAABgBAAQAAAAgBABIgBAAIgCgBg");
	this.shape_27.setTransform(-11.9,-185.6,1.23,1.23);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#EBE7E9").s().p("AA8BgQg3gbgygjQhSg4g0g/IAAAAQAlgHAogFIABACQBQAlBtAHQA2ADAmgEQgdAEgiA9QgSAggRArQgDAGgGADIgGABQgEAAgDgCg");
	this.shape_28.setTransform(-42.8,-183.2,1.23,1.23);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F37D7D").s().p("AgnDEQgzgSgygaIAJj9QADg4ApgnQApgmA4ACQA4ACAnAqQAmApgCA4IgMFEQhRgHhXgeg");
	this.shape_29.setTransform(-37.4,-228.1,1.23,1.23);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F69789").s().p("AgOGEQg4gCgmgqQgngpACg5IATn1QADg4ApgnQApgmA4ACQA4ACAnAqQAmApgCA4IgTH2QgCAugdAjQgcAjgsALQgRAEgRAAIgEAAg");
	this.shape_30.setTransform(-38,-209.1,1.23,1.23);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#EBE7E9").s().p("Ai1AeQgPgPgEgSQgCgKAAgGIAAgBIgBgFQACgGAMgGQAogTCDgJQCAgJA7ANQAeAHAFAJQgCAZgcAaQg3A1h/AKIgnABQhgAAgmgog");
	this.shape_31.setTransform(-40,-191.3,1.23,1.23);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#441A38").s().p("AjMB8QgnAAgZgaQgZgZAHgjQAKgwAXgjQADgFAGgBQAGgCAFAEQAJAFANAMQAQAQAOAEQAKACAlAAQAUAAAdgrQAigzAVgMQATgKAUAEQBeAQBJAyQBEAvAnBBQANAXgPAXQgQAWgeAAg");
	this.shape_32.setTransform(66.4,330.3,1.23,1.23);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#734F53").s().p("AhTBXQgcggACgrQABgVAEgeQAEgeABgVIDRgeQgCAdgEAqQgFAtgBAaQgCAmgZAcQgaAbgkAFIgUABQgrgCgdggg");
	this.shape_33.setTransform(50,326.5,1.23,1.23);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#453C5A").s().p("AAWUKQgGgGAAgIQAerOhIowQhJovjCojQgUg2AZg0QAZg1A2gSQAPgFAMgCQAxgHAqAaQArAZARAvQDHI2BOI9QBNI/gcLaQAAAGgEAEQgEAFgGABIj1AjIgCAAQgHAAgFgEg");
	this.shape_34.setTransform(31.5,156.4,1.23,1.23);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#441A38").s().p("AicB6QgeAAgTgaQgUgbAGghQAFgiALgeQADgHAHgBQAIgBAEAGQAOAUAkAAQAVAAARgRQAIgHAVgfQAng2AwgBQBCAUAzAwQAxAtAbA9QALAYgMAWQgMAXgXAAg");
	this.shape_35.setTransform(-99.2,330.6,1.23,1.23);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#734F53").s().p("AgpB1QgOgEgFgCQgpgQgRgnQgSgnAQgoIAUgvQANgbAIgUIDPAkQgKAdgRAlIgcBCQgOAkggARQgYAOgaAAIgSgBg");
	this.shape_36.setTransform(-107.2,323.5,1.23,1.23);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#453C5A").s().p("AhmUGIj0gqQgHgBgFgIQgEgHADgHQD7qiBnosQBnoqgQpEQgCg5AogqQAngqA6gBQAMgBAPADQAxAIAhAmQAgAlABAyQAQJYhnI4QhoI8j7KtQgCAGgFADQgFACgEAAIgDAAg");
	this.shape_37.setTransform(-76.1,155.7,1.23,1.23);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#D5D2D8").s().p("AGWI9IqCg/QgzgFgsgbQgtgbgbgsQgohAAIhMQACgbAKgdIEOrPQAPgmAlgRQAmgRAmAOQAeAMASAcQASAdgDAhQgCANgEANIkNLPIgCAGQAAAIAEAHQAGAKAMACIKCA+QAGABADAEQAEAFgBAFIgQCpQAAAGgFAEQgEADgEAAIgCAAg");
	this.shape_38.setTransform(22.5,-120.7,1.23,1.23);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#F69789").s().p("AhwDXQhHAAgxgmQgxgnABg2QACgwApgkIA2gyIA0gzQAVgWAVgmQALgTAHgPQAFgMAMgFQANgFAMAFQAMAGAFAMQAFAMgGAMQgRApgcAnQgJANAGAPQAIAOAPAAIAZADQBIAKBFgFIB6gJQAMgBAJAIQAJAIABAMQABAMgIAJQgIAIgMABIinAMQgLABgIAJQgHAJACAMQASB6hLAfQgYALgnABIg3gBg");
	this.shape_39.setTransform(108,-70.7,1.23,1.23);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#F69789").s().p("AAMBTIhsgLIAPiaIBsAKQAgADAVAZQAUAZgDAfQgDAhgZAUQgWASgbAAIgIAAg");
	this.shape_40.setTransform(86.3,-61.5,1.23,1.23);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-142.5,-345.5,285,691.1);


(lib.Interpolación2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#441A38").s().p("AjMB8QgnAAgZgaQgZgZAHgjQAKgwAXgjQADgFAGgBQAGgCAFAEQAJAFANAMQAQAQAOAEQAKACAlAAQAUAAAdgrQAigzAVgMQATgKAUAEQBeAQBJAyQBEAvAnBBQANAXgPAXQgQAWgeAAg");
	this.shape.setTransform(70,326.3,1.23,1.23,-2.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#734F53").s().p("AhTBXQgcggACgrQABgVAEgeQAEgeABgVIDRgeQgCAdgEAqQgFAtgBAaQgCAmgZAcQgaAbgkAFIgUABQgrgCgdggg");
	this.shape_1.setTransform(53.4,323.3,1.23,1.23,-2.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#453C5A").s().p("AAWUKQgGgGAAgIQAerOhIowQhJovjCojQgUg2AZg0QAZg1A2gSQAPgFAMgCQAxgHAqAaQArAZARAvQDHI2BOI9QBNI/gcLaQAAAGgEAEQgEAFgGABIj1AjIgCAAQgHAAgFgEg");
	this.shape_2.setTransform(26.8,154.3,1.23,1.23,-2.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#441A38").s().p("AicB6QgeAAgTgaQgUgbAGghQAFgiALgeQADgHAHgBQAIgBAEAGQAOAUAkAAQAVAAARgRQAIgHAVgfQAng2AwgBQBCAUAzAwQAxAtAbA9QALAYgMAWQgMAXgXAAg");
	this.shape_3.setTransform(-110.4,329.4,1.23,1.23);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#734F53").s().p("AgpB1QgOgEgFgCQgpgQgRgnQgSgnAQgoIAUgvQANgbAIgUIDPAkQgKAdgRAlIgcBCQgOAkggARQgYAOgaAAIgSgBg");
	this.shape_4.setTransform(-118.4,322.3,1.23,1.23);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#453C5A").s().p("AhmUGIj0gqQgHgBgFgIQgEgHADgHQD7qiBnosQBnoqgQpEQgCg5AogqQAngqA6gBQAMgBAPADQAxAIAhAmQAgAlABAyQAQJYhnI4QhoI8j7KtQgCAGgFADQgFACgEAAIgDAAg");
	this.shape_5.setTransform(-87.3,154.5,1.23,1.23);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EBE7E9").s().p("AGVI+IqChBQgzgFgsgcQgsgcgbgsQgng/AHhMQADgdAKgbIERrOQAPgmAlgRQAmgRAmAPQAeALASAdQASAdgEAhQAAANgGAMIkQLOIgBAGQgBAJAEAHQAHAKALABIKCBBQAFABAEAFQAEAEgBAGIgRCoQAAAGgFAEQgEADgFAAIgBAAg");
	this.shape_6.setTransform(-85.9,-118.3,1.23,1.23,-6.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F69789").s().p("AhwDXQhHAAgxgmQgxgnABg2QABgxAqgjIA1gyIA1gzQAigkAZg6QAGgMAMgFQAMgFANAFQAMAGAFAMQAEAMgFAMQgTAsgaAkQgKANAHAOQAIAPAPAAIAZACQBHALBGgFIB6gJQAMgBAJAIQAJAIABAMQABAMgIAJQgIAIgMABIinAMQgMABgHAJQgHAJACALQASB7hLAfQgYALgnABIg3gBg");
	this.shape_7.setTransform(-0.3,-79.4,1.23,1.23,-6.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F69789").s().p("AALBTIhsgLIAQibIBsAMQAgADAVAZQAUAZgDAfQgEAggZAVQgVASgaAAIgKgBg");
	this.shape_8.setTransform(-37.8,-63.4,1.23,1.23,2.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F37D7D").s().p("AgUBgQgfgegOgxQgPgwAJgnQAJgoAbgHQAcgHAcAdQAeAeAPAxQAPAwgKAoQgJAngbAHIgKACQgXAAgWgYg");
	this.shape_9.setTransform(-69.1,-257.4,1.23,1.23,2.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#441A38").s().p("AhtA7QgHgDABgMIABgBIAFguIADgSIACgIIADgHIAEgFIAEgCIABAAIAAgBIDMgOIAEAwIgLAAQgHABgOgQQgPgQgKAAIiUALIgCABIgCABIgBADQgDAIgBASIgCAxQgBAFgCADQgBAAAAAAQAAABgBAAQAAAAgBAAQAAAAgBAAIgCAAg");
	this.shape_10.setTransform(-65.6,-261.1,1.23,1.23,2.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#441A38").s().p("AkPBlQgRgPgHgZIgIgdQgHgKgIgHIgHgGIgEgvIAmgYQAygYBFABQBCABA1AQQAyAPADAAQACgBAugVQAzgYBBgLQBFgLA1ARQAbAIANAKIAEAxQgLAJgJAQIgCAdQgEAagOARQgPASgUACIhtAMIgCAAQgbACgXgSQgXgSgMgfIgNgYQgCgIgMgEQgfgNgbARQgKAGgDAIQgDALgGAOQgHAhgUAVQgVAVgaACIhwAEIgCAAQgTAAgQgOgAi8hGIgLABQgyADgaATQgTAPAAARQAAAfAMApQAGAVANAMQANAMAQgBIBtgEIABAAQAcgCATgaQASgbABglQAAgkgQgQQgXgXhHAAIgUAAgADBhiIgKABQhYAKgVAdQgPATAGAkQAGAkAWAXQAWAYAcgCIBugNQAQgBALgOQALgOADgVQAGgpgFggQgCgRgWgLQgWgMgmAAIgSAAg");
	this.shape_11.setTransform(-11.2,-261.1,1.23,1.23,2.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#B66149").s().p("AnUDqQgyhtABhSQABgnANgiQAOgmAagaQAfgdApgIQAIgCAGgGQAFgGAAgJQgCgrAlgwQAfgoA/gtQArgfBHgSQAtgLBPgKQBogMBPANQBHANA4AWQBxAuA1BCQAhAqAKAyQAKA2gUAsQgLgsgOgdQgQghgYgYQAWAxgCA2QgDBDgpA3QgKgvgbgqQgcgqgngdIgGAAQgXACgfAdQgjAggRAGQgoAQhCgkQgkgUgOgGQgegLgXADQgeADgpAfIgfAZQgTAMgQAGIgGACQAAAAAAAAQAAAAgBABQAAAAAAAAQAAAAAAABIgBACIACAFQAJApgoAzQgpA1ANAqQABAEAPAfQAOAfgJAfQgFARgXAoQgIgfgHgQQgKgagPgQQgSgUgagHQgcgGgVAOQghAXADBDQACAhAEAWQADARALAkQg/hOgrhfg");
	this.shape_12.setTransform(-31,-296,1.23,1.23,2.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#9C4F43").s().p("AAqAVQgQgKgfgBQgXgBgTAEQgHACgGgEQgGgEgBgHQgCgGAEgHQAEgFAHgCQAWgEAXAAQAsABAaARQAGADABAGQACAIgEAGQgFAGgJABQgFAAgFgDg");
	this.shape_13.setTransform(11,-273.3,1.23,1.23,2.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#9C4F43").s().p("Ag+AYQgFgFABgHQAAgHAGgFQAXgUArgGQAYgEAVABQAHABAFAFQAFAFgBAHQAAAHgFAEQgGAFgHAAQgTgBgXAEQgeAFgOANQgFAEgGAAQgIAAgGgGg");
	this.shape_14.setTransform(-35.9,-271.9,1.23,1.23,2.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F4847F").s().p("AACAbQgCgFgJgDQgIgDgIAAQgLgBgIgIQgHgHAAgLQABgMAIgHQAIgIALAAQANAAAQAGQAdAJANAVQAGAJgCAKQgCALgKAHQgGAEgHAAIgDAAQgOAAgIgMg");
	this.shape_15.setTransform(9.6,-246,1.23,1.23,2.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F4847F").s().p("AghAoQgKgFgEgKQgEgLAEgKQALgWAbgNQAMgHAPgDQALgCAKAGQAJAHACALQACALgGAIQgHAJgLADQgHABgIAEQgJAEgCAFQgGAPgQABIgCABQgFAAgGgDg");
	this.shape_16.setTransform(-33.3,-244.9,1.23,1.23,2.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#BE423C").s().p("Ag/ArQgfgdgIgrQgBgJAFgJQAEgHAJgBICngNQAJAAAGAHQAGAHAAAKQgBAsgbAhQgbAhgnACIgCABIgHAAQgiAAgdgag");
	this.shape_17.setTransform(-11.9,-235.6,1.23,1.23,2.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#44203C").s().p("AgQAgQgIgIgBgNIgBgRQgBgNAHgJQAHgJALgBQAKgBAJAIQAIAIABANIABARQABANgHAJQgHAKgLAAIgDAAQgIAAgIgHg");
	this.shape_18.setTransform(-33.4,-259.6,1.23,1.23,2.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#44203C").s().p("AgPAgQgJgIgBgNIgBgRQgBgNAHgJQAHgJALgBQAKgBAJAIQAIAIABANIABARQABANgHAJQgHAKgLAAIgCAAQgJAAgHgHg");
	this.shape_19.setTransform(9,-260.7,1.23,1.23,2.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F69789").s().p("ACKHfQhQgBhfghQhQgchJgxQhIgwg0g8Qg4hCgihRQghhOgHhUIgBgLQgMifB3h6QB3h6C0gNQA2gEA0AIQBQAMBHAnQBHAmAxA5QAuA2AaBMQANAnAFA0IAHBcIAIBXQAFA0gBAjQgDBNglBGQglBIhAAxQhCAyhiAAIgEAAg");
	this.shape_20.setTransform(-31.9,-269.2,1.23,1.23,2.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#441A38").s().p("AhtA7QgHgDABgNIAIhAIAGgPIAEgFIADgCIACAAIAAgBIDMgPIAEAxIgLAAQgHABgPgQQgPgQgJAAIiVALIgCABIgCAEQgDAIgBASIgCAxQgCAKgEAAIgDgBg");
	this.shape_21.setTransform(15.2,-263.2,1.23,1.23,2.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#F8964F").s().p("Ag1BHQgJgGgEgLIgDgJIgFhOQgBgFACgFQACgKAIgIQAJgHALgBIBMgGQALAAAKAGQAJAGAEAKIACAKIAGBNIgBAKQgCALgJAHQgIAIgLAAIhNAGIgDAAQgJAAgIgFgAgpgrQgIAAABAIIAFBOQABAIAHgBIBNgGQADAAACgCQACgDAAgDIgGhOQgBgHgHAAg");
	this.shape_22.setTransform(-13.1,-19.3,1.23,1.23,2.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#754648").s().p("AgeguIAogDIAIAcIAOBDIg3AEg");
	this.shape_23.setTransform(14.3,-19.9,1.23,1.23,2.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#754648").s().p("Ag6gsIBugIQgCAFABAFIAGBOIACAJIhuAIg");
	this.shape_24.setTransform(-28.6,-18.9,1.23,1.23,2.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#754648").s().p("AgaAKIAQg5IArgEIgQBjIgxAEIAGgqg");
	this.shape_25.setTransform(-74,-17.7,1.23,1.23,2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#754648").s().p("AhdgqIDEgOIAHBhIjbAQg");
	this.shape_26.setTransform(-53.9,-18.2,1.23,1.23,2.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#754648").s().p("AgnAqIgGhNIgCgKIBYgGIAHBhIhXAGIAAgKg");
	this.shape_27.setTransform(1.1,-19.6,1.23,1.23,2.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#754648").s().p("AgqArIgGhOQgBgIAIAAIBNgGQAHAAABAHIAFBOQABADgDADQgBACgEAAIhMAGIgCAAQgGAAAAgHg");
	this.shape_28.setTransform(-13.1,-19.3,1.23,1.23,2.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#453C5A").s().p("Ak8DKQhGhSgJh/QgFhOAShOQAIgnAPgnIACgGQAEgLALgBIKsgxQAHgBAFAEQAFAEABAGQAHAfAIAbQANAuADAuIAAAGQAIBrgDAxQgFBSgfA3QgoBFhVAlQhVAmiMAKQgZACgZAAQi3AAhdhsg");
	this.shape_29.setTransform(-30.8,4.5,1.23,1.23,2.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#D1495C").s().p("ABIJKIg9hQQgFgHAAgJQALkxgXk1QgOirgUiJQgDgMgIgGIhahJQAkhAAdgDQAigCAsAvIgCBbQAAAHABAIQAgBZAgCkQA2EdAXF/QAAAHgDAGIgvBaQgDAGgGAAIgCAAQgFAAgEgFg");
	this.shape_30.setTransform(-24.3,-177.5,1.23,1.23,-12.3,0,0,-9.3,-49.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#C9BDC6").s().p("AgqBSQhzhGhPhgQBVBVB5A8QAGADAHgDQAHgCACgHQAQgqASgfQAhg5AagIIAOgBQAjADAvA0QAZAcAcAqIADABQABAegKABQgBAAgxhIQgyhJgeACIgMABIABACIgFgBQgPAKgUAsQgOAcgQAwQgFASgQAIQgHADgHAAIgEAAQgKAAgKgGg");
	this.shape_31.setTransform(-33.5,-179.3,1.23,1.23,2.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#DEDBE1").s().p("AjPL2QgFgGACgFIAGgfQAHgkAQgqQAth4gJiAQgJiBg+hxQgbgygjhJIgthYQhBh5gdhrQgchshNhVIgLgOQgWgkAKgoQAKgoAjgVQBshCDCgjIABAAQAzBABSA4QA0AkA1AbQAGACAHgCQAHgDACgGQARgqASghQAkg/AdgDQAlgDA1A6QAZAcAcArQAAABABAAQAAABAAAAQABAAAAAAQABAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQgHglgTgnQgcg7gxgvQCyAKBnAsQAlARAQAlQAPAmgQAmQguBrALB1QALBzgDB0QgDB0gPBeQgXCLAICMQAICOAlCFQAIAeADAXQACAGgEAEQgDAEgGAAIrTA1QgGAAgEgEg");
	this.shape_32.setTransform(-45.9,-104.1,1.23,1.23,2.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#EBE7E9").s().p("ABKBdQgdgrgZgcQgzg5glACIACgOQgBgTgLgbIA2ACQAwAvAdA6QATAnAHAlQAAABAAAAQAAABAAAAQAAABgBAAQAAAAgBABIgBAAIgCgBg");
	this.shape_33.setTransform(-13.5,-185.1,1.23,1.23,2.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#EBE7E9").s().p("AA8BgQg3gbgygjQhSg4g0g/IAAAAQAlgHAogFIABACQBQAlBtAHQA2ADAmgEQgdAEgiA9QgSAggRArQgDAGgGADIgGABQgEAAgDgCg");
	this.shape_34.setTransform(-44.5,-184.2,1.23,1.23,2.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#F37D7D").s().p("AgnDEQgzgSgygaIAJj9QADg4ApgnQApgmA4ACQA4ACAnAqQAmApgCA4IgMFEQhRgHhXgeg");
	this.shape_35.setTransform(-37,-228.8,1.23,1.23,2.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F69789").s().p("AgOGEQg4gCgmgqQgngpACg5IATn1QADg4ApgnQApgmA4ACQA4ACAnAqQAmApgCA4IgTH2QgCAugdAjQgcAjgsALQgRAEgRAAIgEAAg");
	this.shape_36.setTransform(-38.5,-209.8,1.23,1.23,2.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#EBE7E9").s().p("Ai1AeQgPgPgEgSQgCgKAAgGIAAgBIgBgFQACgGAMgGQAogTCDgJQCAgJA7ANQAeAHAFAJQgCAZgcAaQg3A1h/AKIgnABQhgAAgmgog");
	this.shape_37.setTransform(-41.3,-192.1,1.23,1.23,2.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#D5D2D8").s().p("AGWI9IqCg/QgzgFgsgbQgtgbgbgsQgohAAIhMQACgbAKgdIEOrPQAPgmAlgRQAmgRAmAOQAeAMASAcQASAdgDAhQgCANgEANIkNLPIgCAGQAAAIAEAHQAGAKAMACIKCA+QAGABADAEQAEAFgBAFIgQCpQAAAGgFAEQgEADgEAAIgCAAg");
	this.shape_38.setTransform(25.5,-119.9,1.23,1.23,-4.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#F69789").s().p("AhwDXQhHAAgxgmQgxgnABg2QACgwApgkIA2gyIA0gzQAVgWAVgmQALgTAHgPQAFgMAMgFQANgFAMAFQAMAGAFAMQAFAMgGAMQgRApgcAnQgJANAGAPQAIAOAPAAIAZADQBIAKBFgFIB6gJQAMgBAJAIQAJAIABAMQABAMgIAJQgIAIgMABIinAMQgLABgIAJQgHAJACAMQASB6hLAfQgYALgnABIg3gBg");
	this.shape_39.setTransform(110.7,-81.4,1.23,1.23,-4.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#F69789").s().p("AAMBTIhsgLIAPiaIBsAKQAgADAVAZQAUAZgDAfQgDAhgZAUQgWASgbAAIgIAAg");
	this.shape_40.setTransform(78.7,-63.5,1.23,1.23,2.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-137.6,-345.5,282.6,689.9);


(lib.Interpolación1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EBE7E9").s().p("AGVI+IqChBQgzgFgsgcQgsgcgbgsQgng/AHhMQADgdAKgbIERrOQAPgmAlgRQAmgRAmAPQAeALASAdQASAdgEAhQAAANgGAMIkQLOIgBAGQgBAJAEAHQAHAKALABIKCBBQAFABAEAFQAEAEgBAGIgRCoQAAAGgFAEQgEADgFAAIgBAAg");
	this.shape.setTransform(-88.7,-112.5,1.23,1.23);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F69789").s().p("AhwDXQhHAAgxgmQgxgnABg2QABgxAqgjIA1gyIA1gzQAigkAZg6QAGgMAMgFQAMgFANAFQAMAGAFAMQAEAMgFAMQgTAsgaAkQgKANAHAOQAIAPAPAAIAZACQBHALBGgFIB6gJQAMgBAJAIQAJAIABAMQABAMgIAJQgIAIgMABIinAMQgMABgHAJQgHAJACALQASB7hLAfQgYALgnABIg3gBg");
	this.shape_1.setTransform(-3.2,-62.6,1.23,1.23);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F69789").s().p("AALBTIhsgLIAQibIBsAMQAgADAVAZQAUAZgDAfQgEAggZAVQgVASgaAAIgKgBg");
	this.shape_2.setTransform(-24.9,-53.2,1.23,1.23);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F37D7D").s().p("AgUBgQgfgegOgxQgPgwAJgnQAJgoAbgHQAcgHAcAdQAeAeAPAxQAPAwgKAoQgJAngbAHIgKACQgXAAgWgYg");
	this.shape_3.setTransform(-70.8,-255.2,1.23,1.23);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#441A38").s().p("AhtA7QgHgDABgMIABgBIAFguIADgSIACgIIADgHIAEgFIAEgCIABAAIAAgBIDMgOIAEAwIgLAAQgHABgOgQQgPgQgKAAIiUALIgCABIgCABIgBADQgDAIgBASIgCAxQgBAFgCADQgBAAAAAAQAAABgBAAQAAAAgBAAQAAAAgBAAIgCAAg");
	this.shape_4.setTransform(-67.5,-259.1,1.23,1.23);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#441A38").s().p("AkPBlQgRgPgHgZIgIgdQgHgKgIgHIgHgGIgEgvIAmgYQAygYBFABQBCABA1AQQAyAPADAAQACgBAugVQAzgYBBgLQBFgLA1ARQAbAIANAKIAEAxQgLAJgJAQIgCAdQgEAagOARQgPASgUACIhtAMIgCAAQgbACgXgSQgXgSgMgfIgNgYQgCgIgMgEQgfgNgbARQgKAGgDAIQgDALgGAOQgHAhgUAVQgVAVgaACIhwAEIgCAAQgTAAgQgOgAi8hGIgLABQgyADgaATQgTAPAAARQAAAfAMApQAGAVANAMQANAMAQgBIBtgEIABAAQAcgCATgaQASgbABglQAAgkgQgQQgXgXhHAAIgUAAgADBhiIgKABQhYAKgVAdQgPATAGAkQAGAkAWAXQAWAYAcgCIBugNQAQgBALgOQALgOADgVQAGgpgFggQgCgRgWgLQgWgMgmAAIgSAAg");
	this.shape_5.setTransform(-13.1,-261.6,1.23,1.23);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B66149").s().p("AnUDqQgyhtABhSQABgnANgiQAOgmAagaQAfgdApgIQAIgCAGgGQAFgGAAgJQgCgrAlgwQAfgoA/gtQArgfBHgSQAtgLBPgKQBogMBPANQBHANA4AWQBxAuA1BCQAhAqAKAyQAKA2gUAsQgLgsgOgdQgQghgYgYQAWAxgCA2QgDBDgpA3QgKgvgbgqQgcgqgngdIgGAAQgXACgfAdQgjAggRAGQgoAQhCgkQgkgUgOgGQgegLgXADQgeADgpAfIgfAZQgTAMgQAGIgGACQAAAAAAAAQAAAAgBABQAAAAAAAAQAAAAAAABIgBACIACAFQAJApgoAzQgpA1ANAqQABAEAPAfQAOAfgJAfQgFARgXAoQgIgfgHgQQgKgagPgQQgSgUgagHQgcgGgVAOQghAXADBDQACAhAEAWQADARALAkQg/hOgrhfg");
	this.shape_6.setTransform(-34.5,-295.5,1.23,1.23);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#9C4F43").s().p("AAqAVQgQgKgfgBQgXgBgTAEQgHACgGgEQgGgEgBgHQgCgGAEgHQAEgFAHgCQAWgEAXAAQAsABAaARQAGADABAGQACAIgEAGQgFAGgJABQgFAAgFgDg");
	this.shape_7.setTransform(8.4,-274.8,1.23,1.23);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#9C4F43").s().p("Ag+AYQgFgFABgHQAAgHAGgFQAXgUArgGQAYgEAVABQAHABAFAFQAFAFgBAHQAAAHgFAEQgGAFgHAAQgTgBgXAEQgeAFgOANQgFAEgGAAQgIAAgGgGg");
	this.shape_8.setTransform(-38.4,-271.2,1.23,1.23);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F4847F").s().p("AACAbQgCgFgJgDQgIgDgIAAQgLgBgIgIQgHgHAAgLQABgMAIgHQAIgIALAAQANAAAQAGQAdAJANAVQAGAJgCAKQgCALgKAHQgGAEgHAAIgDAAQgOAAgIgMg");
	this.shape_9.setTransform(8.4,-247.5,1.23,1.23);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F4847F").s().p("AghAoQgKgFgEgKQgEgLAEgKQALgWAbgNQAMgHAPgDQALgCAKAGQAJAHACALQACALgGAIQgHAJgLADQgHABgIAEQgJAEgCAFQgGAPgQABIgCABQgFAAgGgDg");
	this.shape_10.setTransform(-34.4,-244.4,1.23,1.23);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#BE423C").s().p("Ag/ArQgfgdgIgrQgBgJAFgJQAEgHAJgBICngNQAJAAAGAHQAGAHAAAKQgBAsgbAhQgbAhgnACIgCABIgHAAQgiAAgdgag");
	this.shape_11.setTransform(-12.7,-236.2,1.23,1.23);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#44203C").s().p("AgQAgQgIgIgBgNIgBgRQgBgNAHgJQAHgJALgBQAKgBAJAIQAIAIABANIABARQABANgHAJQgHAKgLAAIgDAAQgIAAgIgHg");
	this.shape_12.setTransform(-35.2,-259.1,1.23,1.23);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#44203C").s().p("AgPAgQgJgIgBgNIgBgRQgBgNAHgJQAHgJALgBQAKgBAJAIQAIAIABANIABARQABANgHAJQgHAKgLAAIgCAAQgJAAgHgHg");
	this.shape_13.setTransform(7.1,-262.2,1.23,1.23);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F69789").s().p("ACKHfQhQgBhfghQhQgchJgxQhIgwg0g8Qg4hCgihRQghhOgHhUIgBgLQgMifB3h6QB3h6C0gNQA2gEA0AIQBQAMBHAnQBHAmAxA5QAuA2AaBMQANAnAFA0IAHBcIAIBXQAFA0gBAjQgDBNglBGQglBIhAAxQhCAyhiAAIgEAAg");
	this.shape_14.setTransform(-34.2,-268.8,1.23,1.23);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#441A38").s().p("AhtA7QgHgDABgNIAIhAIAGgPIAEgFIADgCIACAAIAAgBIDMgPIAEAxIgLAAQgHABgPgQQgPgQgJAAIiVALIgCABIgCAEQgDAIgBASIgCAxQgCAKgEAAIgDgBg");
	this.shape_15.setTransform(13.1,-265,1.23,1.23);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F8964F").s().p("Ag1BHQgJgGgEgLIgDgJIgFhOQgBgFACgFQACgKAIgIQAJgHALgBIBMgGQALAAAKAGQAJAGAEAKIACAKIAGBNIgBAKQgCALgJAHQgIAIgLAAIhNAGIgDAAQgJAAgIgFgAgpgrQgIAAABAIIAFBOQABAIAHgBIBNgGQADAAACgCQACgDAAgDIgGhOQgBgHgHAAg");
	this.shape_16.setTransform(-3.6,-20,1.23,1.23);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#754648").s().p("AgeguIAogDIAIAcIAOBDIg3AEg");
	this.shape_17.setTransform(23.7,-21.9,1.23,1.23);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#754648").s().p("Ag6gsIBugIQgCAFABAFIAGBOIACAJIhuAIg");
	this.shape_18.setTransform(-19.1,-18.9,1.23,1.23);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#754648").s().p("AgaAKIAQg5IArgEIgQBjIgxAEIAGgqg");
	this.shape_19.setTransform(-64.4,-15.6,1.23,1.23);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#754648").s().p("AhdgqIDEgOIAHBhIjbAQg");
	this.shape_20.setTransform(-44.4,-17,1.23,1.23);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#754648").s().p("AgnAqIgGhNIgCgKIBYgGIAHBhIhXAGIAAgKg");
	this.shape_21.setTransform(10.5,-21,1.23,1.23);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#754648").s().p("AgqArIgGhOQgBgIAIAAIBNgGQAHAAABAHIAFBOQABADgDADQgBACgEAAIhMAGIgCAAQgGAAAAgHg");
	this.shape_22.setTransform(-3.6,-20,1.23,1.23);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#453C5A").s().p("Ak8DKQhGhSgJh/QgFhOAShOQAIgnAPgnIACgGQAEgLALgBIKsgxQAHgBAFAEQAFAEABAGQAHAfAIAbQANAuADAuIAAAGQAIBrgDAxQgFBSgfA3QgoBFhVAlQhVAmiMAKQgZACgZAAQi3AAhdhsg");
	this.shape_23.setTransform(-20.2,4.6,1.23,1.23);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D1495C").s().p("ABIJKIg9hQQgFgHAAgJQALkxgXk1QgOirgUiJQgDgMgIgGIhahJQAkhAAdgDQAigCAsAvIgCBbQAAAHABAIQAgBZAgCkQA2EdAXF/QAAAHgDAGIgvBaQgDAGgGAAIgCAAQgFAAgEgFg");
	this.shape_24.setTransform(-11,-117.1,1.23,1.23);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#C9BDC6").s().p("AgqBSQhzhGhPhgQBVBVB5A8QAGADAHgDQAHgCACgHQAQgqASgfQAhg5AagIIAOgBQAjADAvA0QAZAcAcAqIADABQABAegKABQgBAAgxhIQgyhJgeACIgMABIABACIgFgBQgPAKgUAsQgOAcgQAwQgFASgQAIQgHADgHAAIgEAAQgKAAgKgGg");
	this.shape_25.setTransform(-31.6,-178.9,1.23,1.23);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#DEDBE1").s().p("AjPL2QgFgGACgFIAGgfQAHgkAQgqQAth4gJiAQgJiBg+hxQgbgygjhJIgthYQhBh5gdhrQgchshNhVIgLgOQgWgkAKgoQAKgoAjgVQBshCDCgjIABAAQAzBABSA4QA0AkA1AbQAGACAHgCQAHgDACgGQARgqASghQAkg/AdgDQAlgDA1A6QAZAcAcArQAAABABAAQAAABAAAAQABAAAAAAQABAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQgHglgTgnQgcg7gxgvQCyAKBnAsQAlARAQAlQAPAmgQAmQguBrALB1QALBzgDB0QgDB0gPBeQgXCLAICMQAICOAlCFQAIAeADAXQACAGgEAEQgDAEgGAAIrTA1QgGAAgEgEg");
	this.shape_26.setTransform(-40.5,-103.2,1.23,1.23);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#EBE7E9").s().p("ABKBdQgdgrgZgcQgzg5glACIACgOQgBgTgLgbIA2ACQAwAvAdA6QATAnAHAlQAAABAAAAQAAABAAAAQAAABgBAAQAAAAgBABIgBAAIgCgBg");
	this.shape_27.setTransform(-11.9,-185.6,1.23,1.23);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#EBE7E9").s().p("AA8BgQg3gbgygjQhSg4g0g/IAAAAQAlgHAogFIABACQBQAlBtAHQA2ADAmgEQgdAEgiA9QgSAggRArQgDAGgGADIgGABQgEAAgDgCg");
	this.shape_28.setTransform(-42.8,-183.2,1.23,1.23);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F37D7D").s().p("AgnDEQgzgSgygaIAJj9QADg4ApgnQApgmA4ACQA4ACAnAqQAmApgCA4IgMFEQhRgHhXgeg");
	this.shape_29.setTransform(-37.4,-228.1,1.23,1.23);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F69789").s().p("AgOGEQg4gCgmgqQgngpACg5IATn1QADg4ApgnQApgmA4ACQA4ACAnAqQAmApgCA4IgTH2QgCAugdAjQgcAjgsALQgRAEgRAAIgEAAg");
	this.shape_30.setTransform(-38,-209.1,1.23,1.23);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#EBE7E9").s().p("Ai1AeQgPgPgEgSQgCgKAAgGIAAgBIgBgFQACgGAMgGQAogTCDgJQCAgJA7ANQAeAHAFAJQgCAZgcAaQg3A1h/AKIgnABQhgAAgmgog");
	this.shape_31.setTransform(-40,-191.3,1.23,1.23);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#441A38").s().p("AjMB8QgnAAgZgaQgZgZAHgjQAKgwAXgjQADgFAGgBQAGgCAFAEQAJAFANAMQAQAQAOAEQAKACAlAAQAUAAAdgrQAigzAVgMQATgKAUAEQBeAQBJAyQBEAvAnBBQANAXgPAXQgQAWgeAAg");
	this.shape_32.setTransform(66.4,330.3,1.23,1.23);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#734F53").s().p("AhTBXQgcggACgrQABgVAEgeQAEgeABgVIDRgeQgCAdgEAqQgFAtgBAaQgCAmgZAcQgaAbgkAFIgUABQgrgCgdggg");
	this.shape_33.setTransform(50,326.5,1.23,1.23);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#453C5A").s().p("AAWUKQgGgGAAgIQAerOhIowQhJovjCojQgUg2AZg0QAZg1A2gSQAPgFAMgCQAxgHAqAaQArAZARAvQDHI2BOI9QBNI/gcLaQAAAGgEAEQgEAFgGABIj1AjIgCAAQgHAAgFgEg");
	this.shape_34.setTransform(31.5,156.4,1.23,1.23);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#441A38").s().p("AicB6QgeAAgTgaQgUgbAGghQAFgiALgeQADgHAHgBQAIgBAEAGQAOAUAkAAQAVAAARgRQAIgHAVgfQAng2AwgBQBCAUAzAwQAxAtAbA9QALAYgMAWQgMAXgXAAg");
	this.shape_35.setTransform(-99.2,330.6,1.23,1.23);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#734F53").s().p("AgpB1QgOgEgFgCQgpgQgRgnQgSgnAQgoIAUgvQANgbAIgUIDPAkQgKAdgRAlIgcBCQgOAkggARQgYAOgaAAIgSgBg");
	this.shape_36.setTransform(-107.2,323.5,1.23,1.23);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#453C5A").s().p("AhmUGIj0gqQgHgBgFgIQgEgHADgHQD7qiBnosQBnoqgQpEQgCg5AogqQAngqA6gBQAMgBAPADQAxAIAhAmQAgAlABAyQAQJYhnI4QhoI8j7KtQgCAGgFADQgFACgEAAIgDAAg");
	this.shape_37.setTransform(-76.1,155.7,1.23,1.23);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#D5D2D8").s().p("AGWI9IqCg/QgzgFgsgbQgtgbgbgsQgohAAIhMQACgbAKgdIEOrPQAPgmAlgRQAmgRAmAOQAeAMASAcQASAdgDAhQgCANgEANIkNLPIgCAGQAAAIAEAHQAGAKAMACIKCA+QAGABADAEQAEAFgBAFIgQCpQAAAGgFAEQgEADgEAAIgCAAg");
	this.shape_38.setTransform(22.5,-120.7,1.23,1.23);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#F69789").s().p("AhwDXQhHAAgxgmQgxgnABg2QACgwApgkIA2gyIA0gzQAVgWAVgmQALgTAHgPQAFgMAMgFQANgFAMAFQAMAGAFAMQAFAMgGAMQgRApgcAnQgJANAGAPQAIAOAPAAIAZADQBIAKBFgFIB6gJQAMgBAJAIQAJAIABAMQABAMgIAJQgIAIgMABIinAMQgLABgIAJQgHAJACAMQASB6hLAfQgYALgnABIg3gBg");
	this.shape_39.setTransform(108,-70.7,1.23,1.23);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#F69789").s().p("AAMBTIhsgLIAPiaIBsAKQAgADAVAZQAUAZgDAfQgDAhgZAUQgWASgbAAIgIAAg");
	this.shape_40.setTransform(86.3,-61.5,1.23,1.23);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-142.5,-345.5,285,691.1);


// stage content:
(lib.Sintítulo6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pins
	this.instance = new lib.Interpolación1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(406.3,388.6);

	this.instance_1 = new lib.Interpolación2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(414.1,388.6);
	this.instance_1._off = true;

	this.instance_2 = new lib.Interpolación3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(406.3,388.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,x:414.1},7).wait(8).to({_off:false,x:406.3},7).to({_off:true,x:414.1},7).wait(8).to({_off:false,x:406.3},7).to({_off:true,x:414.1},7).wait(8).to({_off:false,x:406.3},7).to({_off:true,x:414.1},7).wait(8).to({_off:false,x:406.3},7).to({_off:true,x:414.1},7).wait(8).to({_off:false,x:406.3},7).to({_off:true,x:414.1},7).wait(16).wait(300));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},7).to({_off:true,x:406.3},8).wait(7).to({_off:false,x:414.1},7).to({_off:true,x:406.3},8).wait(7).to({_off:false,x:414.1},7).to({_off:true,x:406.3},8).wait(7).to({_off:false,x:414.1},7).to({_off:true,x:406.3},8).wait(7).to({_off:false,x:414.1},7).to({_off:true,x:406.3},8).wait(7).to({_off:false,x:414.1},7).to({_off:true,x:406.3},8).to({_off:false,x:401.1},7).wait(1).wait(300));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({_off:false},8).to({_off:true},7).wait(7).to({_off:false},8).to({_off:true},7).wait(7).to({_off:false},8).to({_off:true},7).wait(7).to({_off:false},8).to({_off:true},7).wait(7).to({_off:false},8).to({_off:true},7).wait(7).to({_off:false},8).to({_off:true,x:401.1},7).wait(1).wait(300));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(748.8,436.5,285,691.1);
// library properties:
lib.properties = {
	id: '375748E9F0C84BCD8EF5003020556731',
	width: 970,
	height: 787,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['375748E9F0C84BCD8EF5003020556731'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;