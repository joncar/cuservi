var confetti = function(){
	var items = [];
	var colors = ['#3788c7', '#f298be', '#f6e25b', '#8a5bac', '#00a944', '#ec2a28', '#43c4c0', '#f4b92f'];
	var index = 150;
	this.triggered = false;
	var width, height;
	var first = 0;
	var content = undefined;

	width = canvasW;
		height = canvasH;

	var startTime = new Date().getTime();
	var currentTime = startTime;	

	render = function(){
	  var time = new Date().getTime();
	  var delta = (time - currentTime) / (80000/60);

	  
	  /*if(time - startTime > 1500){
	    items[++index % 600] = particle(canvasW/100 * 33, canvasH/100 * 12, index, 20);
	  }*/

	  items.forEach(function(item){
	    item.vx *= (1.0 - 0.05*delta);
	    item.vy += (delta * item.fallSpeed);
	    item.vy /= 1.0 + 0.05*delta;
	    var x = item.x+(delta * item.vx);
	    var y = item.y+(delta * item.vy);
	    var rotate = item.rotate+(delta * item.direction);

	    TweenMax.to(item,item.fallSpeed,{x:x,y:y,rotate:rotate});
	  });				 
	}

	function particle(x, y, i, minv){
	  var angle = Math.random()*(Math.PI*2);
	  var amount = Math.random()*10 + minv;	  
	  var vx = Math.sin(angle) * amount;
	  var vy = Math.cos(angle) * amount;
	  
	  var prop = {
	    x: x,
	    y: y,
	    vx: vx,
	    vy: vy,
	    width: (Math.random() * 10) + 4,
	    height: (Math.random() * 10) + 4,
	    color: colors[i % colors.length],
	    circle: (Math.random() > 0.8),
	    rotate: Math.random() * 180,
	    direction: (Math.random() * 5) - 2.5,
	    fallSpeed: (Math.random() / 10) + 0.1
	  }

	  var element = new createjs.Shape().set(prop);	
	  element.graphics.f(colors[i % colors.length]).rr(0,0,(Math.random() * 10) + 4,(Math.random() * 10) + 4,0);				       			      			      
      return element;
	}

	function tick(event) {	
		stage.update();	
	    render();
	}

	this.triggerConfetti = function(stage){
		content = stage;
		this.triggered = true;
		for(var i = 0; i < index; i++){
			var element = particle(canvasW/100 * 33, canvasH/100 * 12, i, 20);
			content.addChild(element);
		    items[(index + 150 + i) % 600] = element;					    
		}
		index = (index + 150) % 600;

		createjs.Ticker.on("tick", tick);
	}
};