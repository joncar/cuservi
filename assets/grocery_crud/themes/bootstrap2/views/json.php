<?php
    $data = array();
    foreach($list as $num_row=>$row){
        $d = array();
        foreach($columns as $column){
            $d[$column->display_as] = $row->{$column->field_name};
        }     
        $data[] = $d;   
    }
?>
<?php echo json_encode($data) ?>